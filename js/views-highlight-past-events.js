/**
 * @file
 * Views highlight past events behaviors.
 */
(function (Drupal, drupalSettings, once) {
  Drupal.behaviors.viewsHighlightPastEvents = {
    attach: function (context, settings) {
      const dateFieldSelector = settings['views_highlight_past_events']['date_field_selector'];
      let dateElements = once('views-highlight-past-events', dateFieldSelector, context);
      if (dateElements.length === 0) {
        return;
      }

      const highlightType = settings['views_highlight_past_events']['highlight_type'];
      const color = settings['views_highlight_past_events']['color'];
      const customClass = settings['views_highlight_past_events']['custom_class'];
      if (color === '' && customClass === '') {
        return;
      }

      const eventTimestamps = settings['views_highlight_past_events']['event_timestamps'];

      // If a view has the "Table" style display, then the table header element
      // <th> has the same class as the view fields (views-field-{field_id}).
      // We should remove this element from the array, as table's header
      // shouldn't to be highlighted, because it doesn't contain a date.
      if (dateElements[0].nodeName === 'TH') {
        dateElements.shift();
      }
      highLightPastEvents();

      // The recalculation interval in seconds. This is necessary to check if
      // the event has expired if the page with a view has been open for a long
      // time and has not been refreshed.
      const recalculationInterval = settings['views_highlight_past_events']['recalculation_interval'];
      if (recalculationInterval) {
        setInterval(highLightPastEvents, recalculationInterval * 1000);
      }

      /**
       *  If the "Add a custom class to date fields or to view rows if dates are
       *  in the past" option is set, then add a custom CSS class to the view
       *  field or to the row (depend on the settings) so that a user can
       *  highlight it with CSS. If the "Highlight past dates" option is checked
       *  then set the background-color of the view field or the view row
       *  (depend on the settings) to highlight it.
       */
      function highLightPastEvents() {
        // The timestamp of the current time in seconds.
        const currentTimestamp = Date.now() / 1000;
        for (let i = 0; i < eventTimestamps.length; i++) {
          if (eventTimestamps[i] && eventTimestamps[i] <= currentTimestamp) {
            if (color) {
              // Highlight only the date field.
              if (highlightType === 'highlight_field') {
                dateElements[i].setAttribute('style', 'background-color: ' + color);
              }
              // Highlight the entire row
              else if (highlightType === 'highlight_row') {
                dateElements[i].parentElement.style.backgroundColor = color;
              }
            }
            if (customClass) {
              // Add a custom class to the date field.
              if (highlightType === 'highlight_field') {
                dateElements[i].classList.add(customClass);
              }
              // Add a custom class to the entire row.
              else if (highlightType === 'highlight_row') {
                dateElements[i].parentElement.classList.add(customClass);
              }
            }
          }
        }
      }
    }
  };
})(Drupal, drupalSettings, once);
