## CONTENTS OF THIS FILE

- Introduction
- Requirements
- Installation
- Configuration
- Maintainers

## INTRODUCTION

The "Views Highlight Past Events" module provides a user interface that enables
to highlight past events displayed with the Views module. An "event" can be any
entity that has a field of the date type. The main feature of the module is
that checking whether the event has passed is done on the client side using
JavaScript. This avoids using the "Time-based" view caching and thus reduces
server load.

## REQUIREMENTS

This module requires no modules outside of Drupal core.

## INSTALLATION

The installation of this module is like other Drupal modules.

1. If your site is [managed via Composer](https://www.drupal.org/node/2718229),
   use Composer to download the "Views Highlight Past Events" module running
   ```composer require "drupal/views_highlight_past_events"```. Otherwise,
   copy/upload the module to the modules directory of your Drupal installation.

2. Enable the "Views Highlight Past Events" module in 'Extend'.
   (`/admin/modules`)

## CONFIGURATION

1. Add a date field to the view that displays events. In the field settings,
   open the "Highlight past events" section.
2. In the field settings, open the "Highlight past events" section. Set the
   highlight color of past events. Also, it's possible to add a custom CSS
   class that will be applied to a view row if the event passes.
3. Optionally, you can set a "recalculation interval". This enables to
   highlight events that expired during the period the page was opened.
   However, be aware that this does not refresh the view (you can use the
   "Views Auto-Refresh" module for this).

## MAINTAINERS

Andrey Vitushkin (wombatbuddy) - https://www.drupal.org/u/wombatbuddy
